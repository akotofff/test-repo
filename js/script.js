$(document).ready(function () {
    let state;
    let year = [];
    let price = [];
    $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
        var $el = $(this);
        $el.toggleClass('active-dropdown');
        var $parent = $(this).offsetParent(".dropdown-menu");
        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
        }
        var $subMenu = $(this).next(".dropdown-menu");
        $subMenu.toggleClass('show');

        $(this).parent("li").toggleClass('show');

        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
            $('.dropdown-menu .show').removeClass("show");
            $el.removeClass('active-dropdown');
        });

        if (!$parent.parent().hasClass('navbar-nav')) {
            $el.next().css({"top": $el[0].offsetTop, "left": $parent.outerWidth() - 4});
        }

        return false;
    });
    $('.btn-group-state .btn').click(function () {
        $('.btn-group-state .btn').removeClass('active');
        $(this).addClass('active');
        state = $(this).data('state');
    })
    $('.btn-group-years .btn').click(function () {
        let getFullYear = new Date().getFullYear()
        $('.btn-group-years .btn').removeClass('active');
        $(this).addClass('active')
        year = [];
        year.push(getFullYear - $(this).data('year'));
        year.push(getFullYear);
    })
    $('.btn-group-years .btn input').change(function () {
        year = [];
        year.push(+$('#yearStart').val())
        year.push(+$('#yearEnd').val())
    })

    $('.input-group-price input').change(function () {
        price = [];
        price.push(+$('#priceStart').val())
        price.push(+$('#priceEnd').val())
    })

    $('#contact-form').on('submit', function (e) {
        e.preventDefault();
        let mark = $('#markSelect option:selected').val();
        const data = {
            mark: mark,
            state: state,
            year: year,
            price: price
        }
        console.log(data)
        $.ajax({
            type: "POST",
            url: 'https://jsonplaceholder.typicode.com/posts',
            data: data,
            success: (data) => {
                console.log(data)
            },
            error: (err) => {
                console.error(err)
            }
        })
        $('table').removeClass('inActive')
    });
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
});